---
layout: job_page
title: "Issue Triage Specialist"
---

## Responsibilities

* Triage issues on GitLab.com
* Monitor discussions, responds in a timely fashion where appropriate (or
  ensures relevant responders are “nudged” to respond)
* Identify patterns or common issues reported by community
* Fixes community-reported bugs when possible (approximately 20% of the time)
* Develop tools to consolidate and reduce duplicate issues
* Create/manage the moderation and terms of use policies
* Communicate important community concerns to the appropriate team
* Documents answers and improves existing documentation
* Write blog posts relevant to the community
* Establish and report metrics on a regular basis, including recommendations and insights
